﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace Course_work_application_
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
        public static double[,] Read()
        {
            StreamReader f = new StreamReader("C:\\Users\\leysa\\source\\repos\\Course_work(application)\\Course_work(application)\\model1.dat");
            string[] a = f.ReadLine().Split('\t');

            int sn = 0;
            while (!f.EndOfStream)
            {
                string str = f.ReadLine();
                str = str.Replace('.', ',');
                string[] b = str.Split(' ', '\t');
                sn++;
            }
            StreamReader fa = new StreamReader("C:\\Users\\leysa\\source\\repos\\Course_work(application)\\Course_work(application)\\model1.dat");//the path to the file
            string[] aa = fa.ReadLine().Split('\t');

            double[] t = new double[sn];
            double[] p = new double[sn];

            int s = 0;
            while (!fa.EndOfStream)
            {
                string str = fa.ReadLine();
                str = str.Replace('.', ',');
                string[] b = str.Split(' ', '\t');

                t[s] = Convert.ToDouble(b[0]);
                p[s] = Convert.ToDouble(b[1]);

                s++;
            }
            double[,] res = new double[2, s];
            for (int i = 0; i < s; i++)
            {
                res[0, i] = t[i];
                res[1, i] = p[i];
            }
            return res;
        }
        public static double[] Progonka(double[] param)
        {  ////метод прогонки////
            double rc = 0.1, rk = 150, T = 25, H = 20, Q = 10;

            double k = Math.Pow(10, -12) * param[0];
            double mu = Math.Pow(10, -13) * param[1] / 8.64;
            double b1 = param[2];
            double C = param[3];
            double pk = param[4];

            int N = 100;
            int Nt = 99;
            double[] ph = new double[Nt + 1];
            double[] u = new double[N + 1];
            double[] t1 = new double[Nt + 1];
            t1[0] = 0; t1[Nt] = T;
            u[0] = Math.Log(rc);
            u[N] = Math.Log(rk);
            //для разностной схемы
            double h = (u[N] - u[0]) / N;

            for (int i = 1; i < N; i++)
            {
                u[i] = u[0] + i * h;
            }
            double[] e = new double[N + 1];
            for (int i = 1; i <= N; i++)
            {
                e[i] = Math.Exp(2 * u[i]);
            }
            double tay = T / Nt;
            for (int j = 1; j <= Nt; j++)
            {
                t1[j] = t1[j - 1] + tay;
            }

            double[,] pp = new double[N + 1, Nt + 1];
            double l = mu * h * h * b1;
            double[,] a = new double[N + 1, Nt + 1];
            double[,] b = new double[N + 1, Nt + 1];
            double[,] c = new double[N + 1, Nt + 1];
            double[,] d = new double[N + 1, Nt + 1];
            double[,] y = new double[N + 1, Nt + 1];
            double[,] alfa = new double[N + 1, Nt + 1];
            double[,] beta = new double[N + 1, Nt + 1];

            for (int i = 0; i <= N; i++)
                pp[i, 0] = pk; //это когда j = 0
            for (int j = 1; j <= Nt; j++)
            {
                c[0, j] = k * t1[j];
                b[0, j] = -k * t1[j] + C * mu * h / (2 * Math.PI * H);
                d[0, j] = Q * mu * h * t1[j] / (2 * Math.PI * H) + C * mu * h * pp[0, j - 1] / (2 * Math.PI * H);

                for (int i = 1; i <= N; i++)
                {
                    a[i, j] = k * t1[j];
                    b[i, j] = -2 * k * t1[j] - l * e[i];
                    c[i, j] = k * t1[j];
                    d[i, j] = -l * e[i] * pp[i, j - 1];
                }

                y[0, j] = b[0, j];
                alfa[1, j] = c[0, j] / y[0, j];
                beta[1, j] = -d[0, j] / y[0, j];

                for (int i = 1; i < N - 1; i++)
                {
                    y[i, j] = b[i, j] - a[i, j] * alfa[i, j];
                    alfa[i + 1, j] = c[i, j] / y[i, j];
                    beta[i + 1, j] = (-d[i, j] + a[i, j] * beta[i, j]) / y[i, j];
                }
                y[N - 1, j] = b[N - 1, j] - a[N - 1, j] * alfa[N - 1, j];
                beta[N, j] = (-d[N - 1, j] + a[N - 1, j] * beta[N - 1, j]) / y[N - 1, j];
                alfa[N, j] = c[N - 1, j] / y[N - 1, j];

                pp[N, j] = pk;

                for (int i = N - 1; i >= 0; i--)
                    pp[i, j] = alfa[i + 1, j] * pp[i + 1, j] + beta[i + 1, j];
            }
            for (int j = 1; j <= Nt; j++)
                ph[j] = pp[0, j];
            return ph;
        }
        public static double[] T(int Nt)
        {
            double T = 25;
            double[] u = new double[Nt + 1];
            double[] t1 = new double[Nt + 1];
            t1[0] = 0; t1[Nt] = T;
            double tay = T / Nt;
            for (int j = 1; j <= Nt; j++)
            {
                t1[j] = t1[j - 1] + tay;

            }
            return t1;
        }
        public static double[] P_N()
        {
            double[,] x1 = Read();
            int n = x1.GetLength(1);// string length, GetLength(0) - number of lines
            double[] p_n = new double[n];
            for (int i = 0; i < n; i++)
                p_n[i] = x1[1, i];
            return p_n;

        }
        public void Func(double[] param, double[] y, object obj)
        {
            double[] pc = Progonka(param);// это решение прямой задачи - получаем массив pc[0...N] - забойное давление
            double[] p = P_N();
            int n = pc.Length;

            //if (p.Length > n)//разбиения для приведения длин массивов.
            //{
            //    double[] pp = new double[n];
            //    pp[0] = Math.Log(p[0]);
            //    pp[n] = Math.Log(p[p.Length]);
            //    //для разностной схемы
            //    double h = (pp[n] - pp[0]) / n;

            //    for (int i = 1; i < n; i++)
            //    {
            //        pp[i] = pp[0] + i * h;
            //    }
            //    p = pp;
            //}
            for (int i = 0; i < pc.Length; i++)
                y[i] = Math.Pow(p[i] - pc[i], 2); //разность между наблюдаемым и вычисленным давлением
        }
        public int Get_n(double[] param)
        {
            double[] y = Progonka(param);
            int n = y.Length;
            return n;

        }
        public double[] LM()
        {
            double epsg = 0.0000000001;
            double epsf = 0;
            double epsx = 0;
            int maxits = 0;

            alglib.minlmstate state;
            alglib.minlmreport rep;
            double[] bndl = new double[] { 0.001, 1, 0.00001, 0.05, 0 };
            double[] bndu = new double[] { 0.1, double.PositiveInfinity, 0.001, 1, double.PositiveInfinity };

            double[] param = { 0.1, 0.1, 0.1, 0.1, 0.1 };
            int n = Get_n(param);//      (y[i] = Math.Pow(p[i] - pc[i], 2)).length;     
            alglib.minlmcreatev(n, param, 0.0001, out state);
            alglib.minlmsetbc(state, bndl, bndu);
            alglib.minlmsetcond(state, epsg, epsf, epsx, maxits);
            alglib.minlmoptimize(state, Func, null, null);
            alglib.minlmresults(state, out param, out rep);
            return param;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            double[] param = LM();
            textBox1.Text = param[0].ToString();
            textBox2.Text = param[1].ToString();
            textBox3.Text = param[2].ToString();
            textBox4.Text = param[3].ToString();
            textBox5.Text = param[4].ToString();
        }
        private double[] Dp(double[] p_file)
        {
            int sn = p_file.Length;
            double[] t = T(sn);
            int k = 0, j = 0;
            double[] dp = new double[sn];
            int s1 = 0;
            for (int i = 1; i < sn - 3; i++)
            {
                for (int jj = 0; jj < sn - 1 && i + jj < sn - 1; jj++)
                {
                    for (int kk = 0; kk < sn && i - kk > 1; kk++)

                    {
                        if (t[i] != t[j] && t[i] != t[k])

                            if (0.18 <= (Math.Log(t[i + jj] / t[i])) && (Math.Log(t[i + jj] / t[i])) <= 0.22 && 0.18 <= (Math.Log(t[i] / t[i - kk])) && (Math.Log(t[i] / t[i - kk])) <= 0.22)
                            {
                                k = kk; j = jj;

                            }
                            else
                            {
                                k = 1; j = 1;

                            }
                    }

                }
                double ln1 = Math.Log(t[i] / t[i - k]) / (Math.Log(t[i + j] / t[i]) * Math.Log(t[i + j] / t[i - k])) * (p_file[i + j + 1] - p_file[i + j]);
                double ln2 = Math.Log(t[i + j] * t[i - k] / (t[i] * t[i])) / (Math.Log(t[i + j] / t[i]) * Math.Log(t[i] / t[i - k])) * (p_file[i + 1] - p_file[i]);
                double ln3 = Math.Log(t[i + j] / t[i]) / (Math.Log(t[i] / t[i - k]) * Math.Log(t[i + j] / t[i - k])) * (p_file[i - k + 1] - p_file[i - k]);
                dp[s1] = ln1 + ln2 + ln3;

                s1++;
            }
            return dp;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisX.IsLogarithmic = true;
            chart1.ChartAreas[0].AxisY.IsLogarithmic = true;
            chart1.Series[2].ChartType = SeriesChartType.FastPoint; ;
            chart1.Series[3].ChartType = SeriesChartType.FastPoint; ;

            chart1.Series[0].ChartType = SeriesChartType.Line;
            chart1.Series[1].ChartType = SeriesChartType.Line;

            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[1].BorderWidth = 3;
            chart1.Series[0].BorderWidth = 3;
            double[] p_file = P_N();
            double[] param = LM();
            double[] p = Progonka(param);
            double[] dp = Dp(p_file);
            double[] dp1 = Dp(p);
            int s1 = dp.Length;
            int s2 = dp1.Length;
            int sn = p_file.Length;
            int sn1 = p.Length;
            double[] t = T(sn);
            double[] t1 = T(sn1);

            for (int j1 = 2; j1 < s1; j1++)
            {
                chart1.Series[1].Points.AddXY(t[j1], p_file[j1]);
            }
            for (int j1 =5; j1 < s1-10; j1++)
            {
                chart1.Series[0].Points.AddXY(t[j1], dp[j1]);
            }
            for (int j1 = 1; j1 < s2; j1++)
            {
                chart1.Series[3].Points.AddXY(t1[j1], p[j1]);
            }
            for (int j1 = 5; j1 < s2 - 10; j1++)
            {
                chart1.Series[2].Points.AddXY(t1[j1], dp1[j1]);
            }
        }
    }
}
