﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace Course_work_application_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            chart1.Series[0].Points.Clear();
            chart1.Series[0].ChartType = SeriesChartType.Line;
            chart1.Series[1].ChartType = SeriesChartType.Line;
            //chart1.Series[1].ChartType = SeriesChartType.FastPoint; ;
            //chart1.Series[0].ChartType = SeriesChartType.FastPoint; ;
        }
        public double[] Progonka()
        {  ////метод прогонки////
            double rc = 0.1, rk = 150, pk = 8, T = 25, C = 0.1, H = 20, Q = 10;
            double k = Math.Pow(10, -12) * Convert.ToDouble(textBox1.Text);
            double mu = Math.Pow(10, -13) * Convert.ToDouble(textBox3.Text) / 8.64;
            double b1 = Convert.ToDouble(textBox2.Text);

            int N = 200;
            int Nt = 99;
            double[] ph = new double[Nt + 1];
            double[] u = new double[N + 1];
            double[] t1 = new double[Nt + 1];
            t1[0] = 0; t1[Nt] = T;
            u[0] = Math.Log(rc);
            u[N] = Math.Log(rk);
            //для разностной схемы
            double h = (u[N] - u[0]) / N;

            for (int i = 1; i < N; i++)
            {
                u[i] = u[0] + i * h;
            }
            double[] e = new double[N + 1];
            for (int i = 1; i <= N; i++)
            {
                e[i] = Math.Exp(2 * u[i]);
            }
            double tay = T / Nt;
            for (int j = 1; j <= Nt; j++)
            {
                t1[j] = t1[j - 1] + tay;
            }

            double[,] pp = new double[N + 1, Nt + 1];
            double l = mu * h * h * b1;
            double[,] a = new double[N + 1, Nt + 1];
            double[,] b = new double[N + 1, Nt + 1];
            double[,] c = new double[N + 1, Nt + 1];
            double[,] d = new double[N + 1, Nt + 1];
            double[,] y = new double[N + 1, Nt + 1];
            double[,] alfa = new double[N + 1, Nt + 1];
            double[,] beta = new double[N + 1, Nt + 1];

            for (int i = 0; i <= N; i++)
                pp[i, 0] = pk; //это когда j = 0
            for (int j = 1; j <= Nt; j++)
            {
                c[0, j] = k * t1[j];
                b[0, j] = -k * t1[j] + C * mu * h / (2 * Math.PI * H);
                d[0, j] = Q * mu * h * t1[j] / (2 * Math.PI * H) + C * mu * h * pp[0, j - 1] / (2 * Math.PI * H);

                for (int i = 1; i <= N; i++)
                {
                    a[i, j] = k * t1[j];
                    b[i, j] = -2 * k * t1[j] - l * e[i];
                    c[i, j] = k * t1[j];
                    d[i, j] = -l * e[i] * pp[i, j - 1];
                }

                y[0, j] = b[0, j];
                alfa[1, j] = c[0, j] / y[0, j];
                beta[1, j] = -d[0, j] / y[0, j];

                for (int i = 1; i < N - 1; i++)
                {
                    y[i, j] = b[i, j] - a[i, j] * alfa[i, j];
                    alfa[i + 1, j] = c[i, j] / y[i, j];
                    beta[i + 1, j] = (-d[i, j] + a[i, j] * beta[i, j]) / y[i, j];
                }
                y[N - 1, j] = b[N - 1, j] - a[N - 1, j] * alfa[N - 1, j];
                beta[N, j] = (-d[N - 1, j] + a[N - 1, j] * beta[N - 1, j]) / y[N - 1, j];
                alfa[N, j] = c[N - 1, j] / y[N - 1, j];

                pp[N, j] = pk;

                for (int i = N - 1; i >= 0; i--)
                    pp[i, j] = alfa[i + 1, j] * pp[i + 1, j] + beta[i + 1, j];
            }
            for (int j = 0; j <= Nt; j++)
                ph[j] = pp[0, j];
            return ph;
        }

        public double[] T(int Nt)
        {
            double T = 25;
            double[] u = new double[Nt + 1];
            double[] t1 = new double[Nt + 1];
            t1[0] = 0; t1[Nt] = T;
            double tay = T / Nt;
            for (int j = 1; j <= Nt; j++)
            {
                t1[j] = t1[j - 1] + tay;

            }
            return t1;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void обратнаяЗадачаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.Show();
        }

        private void прямаяЗадачаToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.ChartAreas[0].AxisX.IsLogarithmic = false;
            chart1.ChartAreas[0].AxisY.IsLogarithmic = false;
            chart1.Series[0].BorderWidth = 3;
            double[] pp = Progonka();
            int Nt = pp.Length;
            double[] t1 = T(Nt);
            for (int j1 = 0; j1 < Nt - 2; j1++)
            {
                chart1.Series[0].Points.AddXY(t1[j1], pp[j1]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[0].BorderWidth = 3;
            double[] pp = Progonka();
            int Nt = pp.Length;
            double[] t1 = T(Nt);
            chart1.ChartAreas[0].AxisX.IsLogarithmic = true;
            chart1.ChartAreas[0].AxisY.IsLogarithmic = false;

            for (int j1 = 1; j1 < Nt - 2; j1++)
            {
                chart1.Series[0].Points.AddXY(t1[j1], pp[j1] - pp[0]);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisX.IsLogarithmic = true;
            chart1.ChartAreas[0].AxisY.IsLogarithmic = true;
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[1].BorderWidth = 3;
            chart1.Series[0].BorderWidth = 3;
            double[] p = Progonka();
            int sn = p.Length;
            double[] t = T(sn);
            int k = 0, j = 0;
            double[] dp = new double[sn];
            int s1 = 0;
            for (int i = 1; i < sn - 3; i++)
            {
                for (int jj = 0; jj < sn - 1 && i + jj < sn - 1; jj++)
                {
                    for (int kk = 0; kk < sn && i - kk > 1; kk++)

                    {
                        if (t[i] != t[j] && t[i] != t[k])

                            if (0.18 <= (Math.Log(t[i + jj] / t[i])) && (Math.Log(t[i + jj] / t[i])) <= 0.22 && 0.18 <= (Math.Log(t[i] / t[i - kk])) && (Math.Log(t[i] / t[i - kk])) <= 0.22)
                            {
                                k = kk; j = jj;

                            }
                            else
                            {
                                k = 1; j = 1;

                            }
                    }

                }
                double ln1 = Math.Log(t[i] / t[i - k]) / (Math.Log(t[i + j] / t[i]) * Math.Log(t[i + j] / t[i - k])) * (p[i + j + 1] - p[i + j]);
                double ln2 = Math.Log(t[i + j] * t[i - k] / (t[i] * t[i])) / (Math.Log(t[i + j] / t[i]) * Math.Log(t[i] / t[i - k])) * (p[i + 1] - p[i]);
                double ln3 = Math.Log(t[i + j] / t[i]) / (Math.Log(t[i] / t[i - k]) * Math.Log(t[i + j] / t[i - k])) * (p[i - k + 1] - p[i - k]);
                dp[s1] = ln1 + ln2 + ln3;

                s1++;

            }
            for (int j1 = 2; j1 < s1; j1++)
            {
                chart1.Series[0].Points.AddXY(t[j1], dp[j1]);
                chart1.Series[1].Points.AddXY(t[j1], p[j1]);
            }
        }
    
    }
}
